$( document ).ready(
    $("#searchText").on('keyup', function () {
        if($(this).val().length >= 3) {
            $queryString = $(this).val();
            $.ajax({
                type: "GET",
                url: '/site/search?q='+$queryString,
                //dataType: 'json',
                success: function(data) {
                    $('#res').html(data);
                }
            })
        }
    })
);