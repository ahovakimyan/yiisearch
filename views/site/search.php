<h4>
    Found <?=count($data)?> <?=count($data) > 1 ? 'results' : 'result'?>
</h4>

<ul class="list-group">
    <?php foreach($data as $product) { ?>
        <li class="list-group-item">
            <b><?=$product->name ?></b><i style="float: right"><?=$product->category->name ?></i>
            <br><br><p><?=$product->descr ?></p>
        </li>
    <?php } ?>
</ul>
