<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use app\models\Product;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * Displays search page.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionSearch() {
        $request = Yii::$app->request->get();

            $data = Product::find()->filterWhere(['like', 'name', $request['q']])->all();

            return $this->renderAjax("search", [
                'data' => $data
            ]);
    }
}
