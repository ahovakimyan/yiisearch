<?php

use yii\db\Migration;

/**
 * Handles the creation for table `products`.
 */
class m160712_133311_create_products_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'price' => $this->float()->notNull(),
            'descr' => $this->text(),
            'img' => $this->string()->defaultValue('noImg.jpg'),
            'category_id' => $this->integer(),
        ]);

        $this->createIndex('idx-products-category_id','products','category_id');
        $this->addForeignKey('fk-products-categories','products',['category_id'],'categories',['id']);
        $this->createIndex('idx-products-_name','products','name');

        /* -----------------------
            Seeding data
        ----------------------- */
        $seeder = new \tebazil\yii2seeder\Seeder();
        $generator = $seeder->getGeneratorConfigurator();
        $faker = $generator->getFakerConfigurator();

        $seeder->table('products')->columns([
            'id', //automatic pk
            'name'=>$faker->firstName,
            'price'=>$faker->randomFloat(2),
            'descr'=>$faker->text,
            'img'=>'noImg.jpg',
            'category_id' =>$faker->numberBetween(1,20),
        ])->rowQuantity(10000);

        $seeder->refill();
        /* ----------------------- */
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('idx-products-_name','products');
        $this->dropForeignKey('fk-products-categories', 'products');
        $this->dropIndex('idx-products-category_id','products');

        $this->dropTable('products');
    }
}
