<?php

use yii\db\Migration;

/**
 * Handles the creation for table `categories`.
 */
class m160712_133240_create_categories_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('categories', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'descr' => $this->text(),
        ]);

        
        /* -----------------------
            Seeding data
        ----------------------- */
        $seeder = new \tebazil\yii2seeder\Seeder();
        $generator = $seeder->getGeneratorConfigurator();
        $faker = $generator->getFakerConfigurator();

        $seeder->table('categories')->columns([
            'id', //automatic pk
            'name'=>$faker->mimeType,
            'descr'=>$faker->text
        ])->rowQuantity(200);

        $seeder->refill();
        /* ----------------------- */
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('categories');
    }
}
